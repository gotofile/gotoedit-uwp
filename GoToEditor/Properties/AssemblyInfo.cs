﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("GoTo editor (UWP)")]
[assembly: AssemblyDescription("Create, edit, and view GoTo files.")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Marco Benzoni (qlcvea)")]
[assembly: AssemblyProduct("GoTo editor")]
[assembly: AssemblyCopyright("Copyright © Marco Benzoni (qlcvea) 2020")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: ComVisible(false)]
[assembly: NeutralResourcesLanguage("en-US")]
