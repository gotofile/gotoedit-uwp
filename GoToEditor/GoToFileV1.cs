﻿using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace GoToEditor
{
    class GoToFileV1
    {
        private static JsonSerializerSettings serializerSettings = new JsonSerializerSettings
        {
            ContractResolver = new DefaultContractResolver
            {
                NamingStrategy = new CamelCaseNamingStrategy()
            },
            NullValueHandling = NullValueHandling.Ignore
        };

        public int Version { get; set; } = 1;
        public string Url { get; set; } = "";
        public string Notes { get; set; } = null;

        public static GoToFileV1 Deserialize(string json)
        {
            GoToFileV1 result = JsonConvert.DeserializeObject<GoToFileV1>(json, serializerSettings);
            if (result.Version != 1)
            {
                throw new UnsupportedVersionException();
            }
            return result;
        }

        public string Serialize()
        {
            return JsonConvert.SerializeObject(this, serializerSettings);
        }
    }
}
