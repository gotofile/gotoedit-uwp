﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Storage;
using Windows.Storage.AccessCache;
using Windows.System;
using Windows.UI;
using Windows.UI.Core.Preview;
using Windows.UI.Popups;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace GoToEditor
{
    public sealed partial class MainPage : Page
    {
        private enum EditorStatus
        {
            NotEdited, // when a file has been opened. changes to Edited as soon as any field is changed
            Edited, // when any field has been changed from the value corresponding to the current file
            Saving, // when a file is being saved
            Reading // when a file is being read
        };

        private GoToFileV1 fileContent = new GoToFileV1();
        private StorageFile file = null;
        private EditorStatus status = EditorStatus.Reading;
        private EditorStatus Status {
            get {
                return status;
            }
            set
            {
                if (waitingToClose)
                {
                    Application.Current.Exit();
                }
                if (value == status) return; // don't update the window title if it already matches the current state
                status = value;
                UpdateTitle();
            }
        }

        private void UpdateTitle()
        {
            ApplicationView appView = ApplicationView.GetForCurrentView();
            string fileName;
            if (file == null)
            {
                fileName = defaultFilename;
            }
            else
            {
                fileName = file.DisplayName;
            }
            switch (Status)
            {
                case EditorStatus.Edited:
                    IsEnabled = true;
                    appView.Title = string.Format(resourceLoader.GetString("TitleEdited"), fileName);
                    break;
                case EditorStatus.Saving:
                    IsEnabled = false; // disable the window while saving
                    appView.Title = string.Format(resourceLoader.GetString("TitleSaving"), fileName);
                    break;
                case EditorStatus.Reading:
                    IsEnabled = false; // disable the window while reading
                    appView.Title = string.Format(resourceLoader.GetString("TitleReading"), fileName);
                    break;
                default:
                    IsEnabled = true;
                    appView.Title = string.Format(resourceLoader.GetString("TitleNotEdited"), fileName);
                    break;
            }
        }

        private string defaultFilename = "Untitled";
        private Windows.ApplicationModel.Resources.ResourceLoader resourceLoader;
        private bool waitingToClose = false;

        public MainPage()
        {
            IActivatedEventArgs activatedArgs = AppInstance.GetActivatedEventArgs();
            resourceLoader = Windows.ApplicationModel.Resources.ResourceLoader.GetForCurrentView();

            Status = EditorStatus.NotEdited;
            if (activatedArgs.Kind == ActivationKind.File)
            {
                FileActivatedEventArgs fileArgs = (FileActivatedEventArgs)activatedArgs;
                file = (StorageFile)fileArgs.Files[0];
                _ = ReadFile();
            }
            this.InitializeComponent();

            defaultFilename = resourceLoader.GetString("DefaultFilename");

            ApplicationViewTitleBar titleBar = ApplicationView.GetForCurrentView().TitleBar;
            titleBar.BackgroundColor = new Color() { A = 255, R = 0, G = 175, B = 255 };
            titleBar.ForegroundColor = new Color() { A = 255, R = 0, G = 0, B = 0 };
            titleBar.InactiveBackgroundColor = new Color() { A = 255, R = 0, G = 157, B = 240 };
            titleBar.InactiveForegroundColor = new Color() { A = 255, R = 26, G = 26, B = 26 };
            titleBar.ButtonBackgroundColor = titleBar.BackgroundColor;
            titleBar.ButtonHoverBackgroundColor = new Color() { A = 255, R = 51, G = 190, B = 255 };
            titleBar.ButtonPressedBackgroundColor = new Color() { A = 255, R = 102, G = 207, B = 255 };
            titleBar.ButtonInactiveBackgroundColor = titleBar.InactiveBackgroundColor;
            titleBar.ButtonForegroundColor = titleBar.ForegroundColor;
            titleBar.ButtonHoverForegroundColor = new Color() { A = 255, R = 26, G = 26, B = 26 };
            titleBar.ButtonPressedForegroundColor = new Color() { A = 255, R = 38, G = 38, B = 38 };
            titleBar.ButtonInactiveForegroundColor = titleBar.InactiveForegroundColor;

            SystemNavigationManagerPreview.GetForCurrentView().CloseRequested += CloseRequested;
        }

        // When this method is added as a handler to MessageDialog.Completed the application will be closed after the dialog has been dismissed.
        private void DialogCompletedClose(IAsyncOperation<IUICommand> asyncOperation, AsyncStatus asyncStatus)
        {
            Application.Current.Exit();
        }

        private async void CloseRequested(object sender, SystemNavigationCloseRequestedPreviewEventArgs e)
        {
            var deferral = e.GetDeferral();
            if (Status == EditorStatus.Edited)
            {
                var dialog = new MessageDialog(resourceLoader.GetString("UnsavedChangesText"), resourceLoader.GetString("UnsavedChangesTitle"));
                var saveCommand = new UICommand(resourceLoader.GetString("Yes"));
                var closeCommand = new UICommand(resourceLoader.GetString("No"));
                var cancelCommand = new UICommand(resourceLoader.GetString("Cancel"));
                dialog.Commands.Add(saveCommand);
                dialog.Commands.Add(closeCommand);
                dialog.Commands.Add(cancelCommand);
                dialog.DefaultCommandIndex = 0;
                dialog.CancelCommandIndex = 2;
                IUICommand result = await dialog.ShowAsync();
                if (result == cancelCommand)
                {
                    e.Handled = true;
                }
                else if (result == saveCommand)
                {
                    // If saving fails (or the user cancels saving), SaveFile will return false.
                    // In that case, the window should not close.
                    e.Handled = !(await SaveFile(Status));
                }
            }
            else if (Status == EditorStatus.Saving)
            {
                waitingToClose = true;
                e.Handled = true;
                // The value of Status will change when the current operation is finished.
                // At that point the Status setter will read the value of waitingToClose and close the application.
            }
            deferral.Complete();
        }

        private async Task ReadFile()
        {
            Status = EditorStatus.Reading;
            try
            {
                string json = await FileIO.ReadTextAsync(file);
                fileContent = GoToFileV1.Deserialize(json);
            }
            catch (UnsupportedVersionException)
            {
                new MessageDialog(resourceLoader.GetString("UnsupportedError")).ShowAsync().Completed += DialogCompletedClose;
            }
            catch (Exception)
            {
                new MessageDialog(resourceLoader.GetString("UnreadableError")).ShowAsync().Completed += DialogCompletedClose;
            }
            urlTextBox.Text = (fileContent.Url == null) ? "" : fileContent.Url;
            notesTextBox.Text = (fileContent.Notes == null) ? "" : fileContent.Notes;
            Status = EditorStatus.NotEdited;
        }

        private void New_Click(object sender, RoutedEventArgs e)
        {
            LauncherOptions options = new LauncherOptions();
            options.TargetApplicationPackageFamilyName = Package.Current.Id.FamilyName;
            _ = Launcher.LaunchUriAsync(new Uri("gotoedit-uwp://"), options);
        }

        private async void Open_Click(object sender, RoutedEventArgs e)
        {
            await OpenFile();
        }

        private async Task OpenFile()
        {
            var picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
            picker.FileTypeFilter.Add(".goto");
            StorageFile selectedFile = await picker.PickSingleFileAsync();
            if (selectedFile != null)
            {
                LauncherOptions options = new LauncherOptions();
                options.TargetApplicationPackageFamilyName = Package.Current.Id.FamilyName;
                await Launcher.LaunchFileAsync(selectedFile, options);
            }
        }

        private async void Save_Click(object sender, RoutedEventArgs e)
        {
            await SaveFile(Status);
        }

        private async Task<bool> SaveFile(EditorStatus previousStatus)
        {
            if (file == null)
            {
                return await SaveFileAs(previousStatus);
            }
            else
            {
                return await CompleteSave(previousStatus);
            }
        }

        private async void SaveAs_Click(object sender, RoutedEventArgs e)
        {
            await SaveFileAs(Status);
        }

        private async Task<bool> SaveFileAs(EditorStatus previousStatus)
        {
            Status = EditorStatus.Saving;
            if (!Uri.TryCreate(urlTextBox.Text, UriKind.Absolute, out Uri testUri))
            {
                Status = previousStatus;
                _ = new MessageDialog(resourceLoader.GetString("InvalidURLSaveError")).ShowAsync();
                return false;
            }
            var savePicker = new Windows.Storage.Pickers.FileSavePicker();
            savePicker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.DocumentsLibrary;
            savePicker.FileTypeChoices.Add(resourceLoader.GetString("FileType"), new List<string>() { ".goto" });
            savePicker.SuggestedFileName = (file == null) ? defaultFilename + ".goto" : file.Name;
            StorageFile selectedFile = await savePicker.PickSaveFileAsync();
            if (selectedFile != null)
            {
                AppInstance.Unregister();
                AppInstance instance = AppInstance.FindOrRegisterInstanceForKey(selectedFile.Name);
                if (instance.IsCurrentInstance)
                {
                    file = selectedFile;
                    return await CompleteSave(previousStatus);
                }
                else
                {
                    if (file != null)
                    {
                        instance = AppInstance.FindOrRegisterInstanceForKey(file.Name);
                    }
                    // if file != null "instance" is the instance handling the selectedFile
                    // if file == null this instance isn't registered
                    if (file == null || instance.IsCurrentInstance)
                    {
                        _ = new MessageDialog(resourceLoader.GetString("InUseError")).ShowAsync();
                    } else {
                        // this is very unlikely but not impossible
                        instance.RedirectActivationTo();
                        new MessageDialog(resourceLoader.GetString("InUseUnrecoverableError")).ShowAsync().Completed += DialogCompletedClose;
                    }
                    Status = previousStatus;
                    return false;
                }     
            }
            else
            {
                Status = previousStatus;
                return false;
            }
        }

        private async Task<bool> CompleteSave(EditorStatus previousStatus)
        {
            Status = EditorStatus.Saving;
            if (!Uri.TryCreate(urlTextBox.Text, UriKind.Absolute, out Uri testUri))
            {
                Status = previousStatus;
                _ = new MessageDialog(resourceLoader.GetString("InvalidURLSaveError")).ShowAsync();
                return false;
            }
            Windows.Storage.Provider.FileUpdateStatus fileStatus;
            try
            {
                CachedFileManager.DeferUpdates(file);
                await FileIO.WriteTextAsync(file, fileContent.Serialize());
                fileStatus = await CachedFileManager.CompleteUpdatesAsync(file);
            }
            catch (Exception)
            {
                Status = previousStatus;
                _ = new MessageDialog(resourceLoader.GetString("GenericSaveError")).ShowAsync();
                return false;
            }
            if (fileStatus == Windows.Storage.Provider.FileUpdateStatus.Complete)
            {
                Status = EditorStatus.NotEdited;
                return true;
            }
            else
            {
                Status = previousStatus;
                _ = new MessageDialog(resourceLoader.GetString("GenericSaveError")).ShowAsync();
                return false;
            }
        }

        private void OpenLinkConfirmation(IUICommand command)
        {
            _ = Launcher.LaunchUriAsync((Uri)command.Id);
        }

        private void OpenLink_Click(object sender, RoutedEventArgs e)
        {
            Uri uri;
            try
            {
                uri = new Uri(fileContent.Url);
            }
            catch (Exception)
            {
                _ = new MessageDialog(resourceLoader.GetString("InvalidURL")).ShowAsync();
                return;
            }

            string scheme = uri.Scheme.ToLower();

            // if the scheme is not in this list the user will be prompted for confirmation
            string[] safeOpenSchemes =
            {
                "http", "https"
            };

            if (safeOpenSchemes.Contains(scheme))
            {
                _ = Launcher.LaunchUriAsync(uri);
            }
            else
            {
                var messageDialog = new MessageDialog(resourceLoader.GetString("UnsafeSchemeText"), resourceLoader.GetString("UnsafeSchemeTitle"));
                UICommandInvokedHandler handler = new UICommandInvokedHandler(OpenLinkConfirmation);
                messageDialog.Commands.Add(new UICommand(resourceLoader.GetString("Yes"), new UICommandInvokedHandler(OpenLinkConfirmation), uri));
                messageDialog.Commands.Add(new UICommand(resourceLoader.GetString("No")));
                messageDialog.DefaultCommandIndex = 1;
                messageDialog.CancelCommandIndex = 1;
                _ = messageDialog.ShowAsync();
            }
        }

        private void urlTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Status = EditorStatus.Edited;
            fileContent.Url = urlTextBox.Text;
        }

        private void notesTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            Status = EditorStatus.Edited;
            fileContent.Notes = (notesTextBox.Text == "") ? null : notesTextBox.Text;
        }

        private async void About_Click(object sender, RoutedEventArgs e)
        {
            _ = Launcher.LaunchFileAsync(await StorageFile.GetFileFromApplicationUriAsync(new Uri("ms-appx:///About.txt")));
        }
    }
}
