﻿using System;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Storage;

namespace GoToEditor
{
    public static class Program
    {
        // This project includes DISABLE_XAML_GENERATED_MAIN in the build properties,
        // which prevents the build system from generating the default Main method:
        //static void Main(string[] args)
        //{
        //    global::Windows.UI.Xaml.Application.Start((p) => new App());
        //}

        static void Main(string[] args)
        {
            IActivatedEventArgs activatedArgs = AppInstance.GetActivatedEventArgs();

            if (activatedArgs.Kind == ActivationKind.File)
            {
                FileActivatedEventArgs fileArgs = (FileActivatedEventArgs)activatedArgs;
                if (fileArgs.Files.Count == 1)
                {
                    StorageFile file = (StorageFile)fileArgs.Files[0];
                    AppInstance instance = AppInstance.FindOrRegisterInstanceForKey(file.Name);
                    if (instance.IsCurrentInstance)
                    {
                        global::Windows.UI.Xaml.Application.Start((p) => new App());
                    }
                    else
                    {
                        instance.RedirectActivationTo();
                    }
                }
                else
                {
                    throw new NotImplementedException();
                }
            }

            if (AppInstance.RecommendedInstance != null)
            {
                AppInstance.RecommendedInstance.RedirectActivationTo();
            }
            else
            {
                global::Windows.UI.Xaml.Application.Start((p) => new App());
            }
        }
    }
}
